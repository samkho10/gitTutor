# gitTutor
Learn your favorite languages from your favorite people!

## Usage
Simply clone the repo and run:
```bash
node index.js
```
and it should be running in your port 3000.
